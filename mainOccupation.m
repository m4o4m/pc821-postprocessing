close all
clear
clc   
addpath('./functions');   

%% Flags
    flag.plotMeasurements   = (true);
    flag.plotOccupation     = (true); 
%% Load Measurement Files   
    loadFiles    
%% Post Processing
    postprocessing
%% Plot Measurements         
    plotMeasurements
%% Analytic LQG Solution   
    analyticSolution    
%% Plot Occupation    
    plotOccupation