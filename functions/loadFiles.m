

%% File Selection   
    measurement.directory = 'measurement/'; 
    [measurement.name,measurement.path] = uigetfile(fullfile(measurement.directory,'*.bin'), ...
            'Select One or More Files','MultiSelect', 'on');
    if ~iscell(measurement.name),measurement.name=cellstr(measurement.name);end
    addpath(measurement.path);       
    measurement.num = numel(measurement.name);  
    
 for i=1:measurement.num   
%% Load JSON            
    %check for json param file
    [~,tmp.name,~]=fileparts(measurement.name{i});
    tmp.json.name{i} = strcat(tmp.name,'.json');
    tmp.json.path{i} = measurement.path;
    if isfile(strcat(tmp.json.path{i},tmp.json.name{i}))     
        measurement.params{i} = jsondecode(fileread(strcat(tmp.json.path{i},tmp.json.name{i})));  
    else
        error('measurement:missingParams','No parameter file found in for measurement file \n %s',measurement.name{i})   
    end        
    
%% Load Data 
    tmp.fileName = strcat(measurement.name{i});
    tmp.fileID = fopen(tmp.fileName, 'r');
    if tmp.fileID == -1, error('Cannot open file: %s', tmp.fileName); end
    tmp.format = 'int16';
    tmp.Data = fread(tmp.fileID, [numel(measurement.params{i}.measurement.channels),Inf], tmp.format);
    tmp.close = fclose(tmp.fileID);   
    
%% Extract Channels
    %extract the channel number and saves it into 'idx' struct for indexing
    %signals in postprocessing  
    
    %list of used channel numbers    
    idx.list = find(~contains(measurement.params{i}.measurement.channels,'OFF')); 
    idx.num = numel(idx.list);       
    
    for j=1:idx.num
        idx.nameList(j) = measurement.params{i}.measurement.channels(idx.list(j));    
               
        %measurement signal - 'Y' 
        if strcmp(idx.nameList(j),'Y'); idx.y = j; end       
        %position estimation - 'X1'
        if strcmp(idx.nameList(j),'X1'); idx.x1 = j; end
        %velocity estimation - 'X2' 
        if strcmp(idx.nameList(j),'X2'); idx.x2 = j; end       
        %kalman covariance element p11 - 'P11'
        if strcmp(idx.nameList(j),'P11'); idx.p11 = j; end
        %kalman covariance element p22 - 'P22'
        if strcmp(idx.nameList(j),'P22'); idx.p22 = j; end       
        %kalman covariance element p12 - 'P12'
        if strcmp(idx.nameList(j),'P12'); idx.p12 = j; end
        %linear feedback - 'UFB' 
        if strcmp(idx.nameList(j),'UFB'); idx.ufb = j; end       
        %modulation (kalman) - 'UP'
        if strcmp(idx.nameList(j),'UP'); idx.up = j; end
        %modulation (DAC) - 'UP_DAC'
        if strcmp(idx.nameList(j),'UP_DAC'); idx.up_dac = j; end        
        %innovation - 'INNO'
        if strcmp(idx.nameList(j),'INNO'); idx.inno = j; end
        %freefall trigger - 'FREEFALL' 
        if strcmp(idx.nameList(j),'FREEFALL'); idx.freefall = j; end       
        %fast switch trigger - 'SWITCH'
        if strcmp(idx.nameList(j),'SWITCH'); idx.switch = j; end
        %status - 'STATUS'
        if strcmp(idx.nameList(j),'STATUS'); idx.status = j; end        
    end    
    
%% Data
    %save data to 'measurement' stucture
    if isfield(idx,'y');            measurement.Data{i,idx.y} = tmp.Data(idx.list(idx.y),:); end
    if isfield(idx,'x1');           measurement.Data{i,idx.x1} = tmp.Data(idx.list(idx.x1),:); end
    if isfield(idx,'x2');           measurement.Data{i,idx.x2} = tmp.Data(idx.list(idx.x2),:); end
    if isfield(idx,'p11');          measurement.Data{i,idx.p11} = tmp.Data(idx.list(idx.p11),:); end
    if isfield(idx,'p22');          measurement.Data{i,idx.p22} = tmp.Data(idx.list(idx.p22),:); end
    if isfield(idx,'p12');          measurement.Data{i,idx.p12} = tmp.Data(idx.list(idx.p12),:); end
    if isfield(idx,'ufb');          measurement.Data{i,idx.ufb} = tmp.Data(idx.list(idx.ufb),:); end
    if isfield(idx,'up');           measurement.Data{i,idx.up} = tmp.Data(idx.list(idx.up),:); end
    if isfield(idx,'up_dac');       measurement.Data{i,idx.up_dac} = tmp.Data(idx.list(idx.up_dac),:); end
    if isfield(idx,'inno');         measurement.Data{i,idx.inno} = tmp.Data(idx.list(idx.inno),:); end
    if isfield(idx,'free_fall');    measurement.Data{i,idx.freefall} = tmp.Data(idx.list(idx.freefall),:); end
    if isfield(idx,'switch');       measurement.Data{i,idx.switch} = tmp.Data(idx.list(idx.switch),:); end
    if isfield(idx,'status');       measurement.Data{i,idx.status} = tmp.Data(idx.list(idx.status),:); end    
 end    
 
 clear i j tmp 