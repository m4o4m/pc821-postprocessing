%% Plot 

if flag.plotMeasurements
    for i=1:measurement.num 
        tmp.f = figure;
            set(tmp.f,'Name',measurement.name{i})   
        tmp.tlo = tiledlayout(tmp.f,'flow');
        for j = 1:idx.num
            nexttile;
            plot(measurement.Time{i,j},measurement.Data{i,j},'.');  
            hold on;
            grid on;
            title(idx.nameList(j),'interpreter','latex')
            xlabel('Samples','interpreter','latex')
            ylabel('y','interpreter','latex')
                set(gca,'TickLabelInterpreter','latex')  
        end     
        tmp.f2 = figure;
            set(tmp.f2,'Name',measurement.name{i})   
        tmp.tlo = tiledlayout(tmp.f2,'flow');
        for j = 1:numel(idx.psdList)
            nexttile;
            plot(measurement.f{i,idx.psdList(j)},10*log10(measurement.pxx{i,idx.psdList(j)}),'.');
            grid on;
            title(idx.nameList(idx.psdList(j)),'interpreter','latex')     
            xlabel('Frequency','interpreter','latex')
            ylabel('$$S_{xx}$$','interpreter','latex')
                set(gca,'TickLabelInterpreter','latex')
        end   
    end
end
clear tmp i j