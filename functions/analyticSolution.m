%% Occupation LQG Theory
    %calculate the theoretical occupation based on the model parameters.
    %parameters are taken from the first file. 

%% Extracting Parameters from JSON
    
    p.dt    = 1/measurement.params{1}.parameter.fs;
    p.w     = measurement.params{1}.parameter.omega; 
    p.r     = measurement.params{1}.parameter.radius;
    p.mbar  = measurement.params{1}.parameter.mbar;
    p.mass  = measurement.params{1}.parameter.mass;    
    p.qzpf = measurement.params{1}.parameter.qzpf;   
    
    p.kalman.R  = measurement.params{1}.parameter.measurement_noise;    
    p.kalman.Q  = measurement.params{1}.parameter.force_noise;  

    %new version: 
    if isfield(measurement.params{1}.parameter, 'd')        
        p.d = measurement.params{1}.parameter.d;
    %old version:    
    else        
        p.mgas  = 0.028996;
        p.R     = 8.314;
        p.T0    = measurement.params{1}.parameter.t0;
        p.dgas  = 64/3*p.r^2*p.mbar*100 / (p.mass*sqrt(p.R*p.T0/(pi*p.mgas)));
        p.d     = p.dgas + measurement.params{1}.parameter.d_eff;  
    end   
     
    %axis 
    occupation.theory.gain = 0:500:100e3;
for j=1:numel(occupation.theory.gain)            
    %% Model
    %system matrix
    p.model.A               = [0,p.w;-p.w,-p.d];
    %input matrix feedback
    p.model.B               = [0;1];
    %output matrix
    p.model.C               = [1,0];
    %feedthrough
    p.model.D               = 0;
    %input matrix thermal noise
    p.model.G               = [0; 1]; 
    %continuous state space system    
    sys.contSystem          = ss(p.model.A,[p.model.B p.model.G],p.model.C,p.model.D);    
    
    %kalman        
    [~,p.kalman.L,p.kalman.P] = kalman(sys.contSystem ,p.kalman.Q,p.kalman.R,0);
    occupation.theory.nKal(j) = p.kalman.P(1,1)*p.dt/2-0.5; 
    
    if occupation.theory.gain(j) == 0
        occupation.theory.gain(j) = 1;
    end
    %lqr
    p.control.Q = diag([p.w/2, p.w/2]);    
    p.control.R = p.w/((occupation.theory.gain(j)*2*pi)^2);  
    [p.lqr.K,~,~] = lqr(p.model.A,p.model.B,p.control.Q,p.control.R);      

    %closed loop ricatti       
    tmp.A = [p.model.A - p.model.B*p.lqr.K , p.model.B*p.lqr.K ; zeros(2,2),p.model.A - p.kalman.L*p.model.C] ;
    tmp.B = [p.model.G, zeros(2,1); p.model.G, -p.kalman.L];
    tmp.N = diag([p.kalman.Q,p.kalman.R]);
    tmp.Q = tmp.B*tmp.N*tmp.B';
    tmp.P = lyap(tmp.A,tmp.Q);    
    %occupation
    occupation.theory.n(j) = tmp.P(1,1)*p.dt/2-0.5;     
end

clear j p tmp sys 