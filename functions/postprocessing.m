
%% 
 for i=1:measurement.num 
     
%% Conversion
    %convert existing signals
    
    %measurement signal
    if isfield(idx,'y')
        %conversion: quantization steps -> volts -> zpf    
        measurement.Data{i,idx.y} =  measurement.Data{i,idx.y} * measurement.params{i}.parameter.cmv / 2^measurement.params{i}.measurement.numBit / measurement.params{i}.parameter.qzpf;  
    end
    %position estimation
    if isfield(idx,'x1')
        %conversion: quantization steps -> volts -> zpf
        measurement.Data{i,idx.x1} =  measurement.Data{i,idx.x1} * measurement.params{i}.parameter.cmv / 2^measurement.params{i}.measurement.numBit / measurement.params{i}.parameter.qzpf;
    end
    %velocity estimation
    if isfield(idx,'x2') 
        %conversion: quantization steps -> volts -> zpf
        measurement.Data{i,idx.x2} =  measurement.Data{i,idx.x2} * measurement.params{i}.parameter.cmv / 2^measurement.params{i}.measurement.numBit / measurement.params{i}.parameter.qzpf;  
    end    
    %kalman covariance 
    if isfield(idx,'p11') 
        %conversion: quantization steps -> zpf
        measurement.Data{i,idx.p11} =  measurement.Data{i,idx.p11} / 2^10;   
    end
    %kalman covariance 
    if isfield(idx,'p22') 
        %conversion: quantization steps -> zpf
        measurement.Data{i,idx.p22} =  measurement.Data{i,idx.p22} / 2^10;   
    end
    %kalman covariance 
    if isfield(idx,'p11') 
        %conversion: quantization steps -> zpf
        measurement.Data{i,idx.p12} =  measurement.Data{i,idx.p12} / 2^10;   
    end
    %linear feedback
    if isfield(idx,'ufb') 
        %conversion: quantization steps -> volts -> zpf
        measurement.Data{i,idx.ufb} =  measurement.Data{i,idx.ufb} * measurement.params{i}.parameter.cmv / 2^measurement.params{i}.measurement.numBit / measurement.params{i}.parameter.qzpf;  
    end   
    %modulation (kalman)
    if isfield(idx,'up') 
        %conversion: quantization steps -> volts -> zpf
        measurement.Data{i,idx.up} =  measurement.Data{i,idx.up} * measurement.params{i}.parameter.cmv / 2^measurement.params{i}.measurement.numBit / measurement.params{i}.parameter.qzpf;  
    end 
    %modulation (DAC)
    if isfield(idx,'up_dac') 
        %conversion: quantization steps -> volts -> zpf
        measurement.Data{i,idx.up_dac} =  measurement.Data{i,idx.up_dacc} * measurement.params{i}.parameter.cmv / 2^measurement.params{i}.measurement.numBit / measurement.params{i}.parameter.qzpf;  
    end    
    %innovation
    if isfield(idx,'inno') 
        %conversion: quantization steps -> volts -> zpf
        measurement.Data{i,idx.inno} =  measurement.Data{i,idx.inno} * measurement.params{i}.parameter.cmv / 2^measurement.params{i}.measurement.numBit / measurement.params{i}.parameter.qzpf;  
    end   
    %freefall trigger
    if isfield(idx,'free_fall') 
        %conversion: quantization steps -> volts
        measurement.Data{i,idx.freefall} =  measurement.Data{i,idx.freefall} / 2^32;   
    end
    %fast switch trigger
    if isfield(idx,'switch') 
        %conversion: quantization steps -> volts
        measurement.Data{i,idx.switch} =  measurement.Data{i,idx.switch} / 2^32;   
    end
    %status flag
    if isfield(idx,'status') 
        %conversion: quantization steps -> volts
        measurement.Data{i,idx.status} =  measurement.Data{i,idx.status} / 2^32;   
    end      

%% Post Processing

    %list of signals for PSD
    idx.psdList = [idx.y, idx.x1, idx.x2, idx.inno, idx.ufb];
    
    for k = 1:numel(idx.psdList)
        %psd
        [measurement.pxx{i,idx.psdList(k)},measurement.f{i,idx.psdList(k)}] = phiPSD(measurement.Data{i,idx.psdList(k)},1/measurement.params{i}.measurement.samplingTime,10,3e5);         
    end
        
    %create time vector for plots
    for j=1:idx.num                
        measurement.Time{i,j} = 0 : measurement.params{i}.measurement.samplingTime : (size(measurement.Data{i,j},2) -1) * measurement.params{i}.measurement.samplingTime; 
    end  
    
%% Occupation Homodyne    

    %occupation based on kalman, infinity gain occupation
    occupation.homodyne.nKalman(i) = (mean(measurement.Data{i,idx.p11})/2)-0.5;  
    
    %sum of occupation P11 and <z z^T>
    occupation.homodyne.n(i) = (mean(measurement.Data{i,idx.p11}) + var(measurement.Data{i,idx.x1}(ceil(numel(measurement.Data{i,idx.x1}))/2:end)))/2 - 0.5;       

    %axis for plot
    occupation.homodyne.gain(i) = measurement.params{i}.parameter.gain;
    
 end 
 clear i j k