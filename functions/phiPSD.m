function [pxx,f] = phiPSD(data,varargin)
% phiPSD.m
%       Power spectral desity for input data with sampling frequency Fs.    
% 
% Author: Philipp Rosenzweig
% Email: rosenzweig@acin.tuwien.ac.at
%
% Institut f�r Automatisierungs- und Regelungstechnik (ACIN)
% Technische Universit�t Wien
% Gu�hausstra�e 27-29 / E376/
% A-1040 Wien, �sterreich
%
% Februar 2020; Last revision: 18-02-2020
% Version 2.0
%---------------------------

    numvarargs = length(varargin);
    if numvarargs > 3
        error('phiPSD:TooManyInputs', ...
            'requires at most 3 optional inputs');
    end

    % set defaults for optional inputs
    optargs = {12.5e6 100 3e5};       
    [optargs{1:numvarargs}] = varargin{:};
    Fs          = optargs{1};
    n           = optargs{2};
    maxFreq     = optargs{3};
    %time domain signal
    x = data;
    %pwelch setup
    numSegments = n;
    nsc = floor(length(x)/numSegments);
    nov = floor(nsc/2);
    nfft = 2^nextpow2(nsc);

    %pwelch
    [pxx,f] = pwelch(x,hamming(nsc),nov,nfft,Fs);            
    
    %cut
    idx     = find(f> maxFreq,1);
    pxxCut  = pxx(1:idx);
    fCut    = f(1:idx);

    pxx = pxxCut;
    f = fCut;    
return