%% Plot 

if flag.plotOccupation
    
    tmp.f = figure;
        set(tmp.f,'Name','Occupation')   

    tmp.tlo = tiledlayout(tmp.f,'flow');

    nexttile; 
    tmp.p1 = plot(occupation.theory.gain,occupation.theory.n);
        set(tmp.p1,'LineWidth',2,'LineStyle','--','Color',[0.5 0.5 0.5],'DisplayName', 'LQG Theory')
    hold on;
    grid on;
    
    tmp.p2 = plot(occupation.theory.gain,occupation.theory.nKal);
        set(tmp.p2,'LineWidth',2,'LineStyle',':','Color',[0.5 0.5 0.5],'DisplayName', '$\infty$-gain Occupation')
        
    tmp.p3 = plot(occupation.homodyne.gain,occupation.homodyne.n,'o'); 
        set(tmp.p3,'LineWidth',2,'Color',[0 0 0.7],'MarkerSize',8, 'DisplayName', 'Homodyne Occupation')
        
    xlim([0 20e3])
    ylim([0 15])
        
    xlabel('Gain (Hz)','interpreter','latex')
    ylabel('Occupation','interpreter','latex')
        set(gca,'TickLabelInterpreter','latex')  

    tmp.lgd = legend;
        set(tmp.lgd, 'Location', 'North','interpreter','latex')
        
end
clear tmp