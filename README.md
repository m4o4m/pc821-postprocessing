# Squeezing | Matlab
 
This project aims to cool levitated nanoparticle in the ground state and produce squeezed by parametric modulation.
For that, a time-variant Kalman filter with delay compensation in the form of a least mean square predictor is implemented on the `PC821 + FMC120` for real-time estimation of the quantum state of the levitated nanoparticle.
By parametric modulation through an onboard harmonic drive, squeezed states can be produced. 
The sequencing star allows to choose between different protocols with different settings, e.g., squeezing, free fall, or fast switch of the optical trap.

This project contains firmware and software files for `PC821 + FMC120`, as well as Matlab/Simulink files to visualize data captured by the onboard memory and to create new IP cores.
Included are all necessary Matlab/Simulink files, StellaIP files, Xilinx Vivado project,and the C++ interface and GUI (QT).
Each project has a directory, containing all firmware related files, directories for the software applications, and the Matlab project.

---

## plotOccupation.m

The Matlab script plotOccupation.m plots the PSD and time trace of chosen measurement files. It also computes the occupation for each measurement as well as the occupation obtained by the analytic solution of the LQG.
Select one or more measurement files (.bin) in the measurement directory and load it into Matlab together with the corresponding .json file, which contains all necessary information concerning the measurement file, e.g., parameter, signals, or settings.
The PSD and occupation of the measurement data is calculated. Flags in the main file can toggle plotting all data and the occupation plot.


---

## Release Notes

**Date:** 04/02/2024<br>
**Release notes:** initial design<br>

**Design revision:** v1.0<br>
**Vivado version:** 2020.1<br>
**Matlab version:** 2019b <br>
**Windows version:** Windows 10<br>
**StellarIP version:** 1.3.22.0<br>
**Test Platform:** PC821 (KU085) + FMC120<br>

---
## Log

- **04. February 2024 | CID 314 | v1.0**: initial commit

---

## Author

Philipp Rosenzweig<br>
Automation and Control Institute | TU Wien <br>
mail@philipp-rosenzweig.com